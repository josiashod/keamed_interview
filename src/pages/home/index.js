import React from "react";
import Chart from "react-apexcharts";

export default function Index()
{
    const series_donut = [76, 100 - 76]

    const options_donut = {
        dataLabels: {
            enabled: false
        },
        legend:{
            show: false,
        },
        colors:['#FCBF9A', '#CBDBE5']
        // plotOptions: {
        //     pie: {
        //       donut: {
        //         labels: {
        //           show: true,
        //         }
        //       }
        //     }
        // }
    }

    const options = {
        chart: {
          id: "basic-bar"
        },
        xaxis: {
          categories: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        },
        yaxis:{
            labels: {
                formatter: (value) => { return value + "M" },
            }
        },
        colors:['#2074BB', '#E91E63'],
        stroke: {curve: 'smooth'}
    }

    const series = [
        {
          name: "series-1",
          data: [140, 145, 155, 220, 165, 155, 170, 91, 155, 170, 155, 170]
        },
        {
        name: "series-2",
        data: [140, 165, 155, 170, 91, 155, 170, 145, 155, 220, 155, 170]
        }
    ]
    
    return (
        <div>
            
            <div className="block p-6 bg-white border border-gray-200 rounded-lg shadow-sm mb-4">
                <p className="font-normal text-primary">Dernière connexion: 23/11/21</p>
            </div>

            <div className="flex mb-4 lg:space-x-4 space-y-4 lg:space-y-0">
                <div className="w-full lg:w-3/5 bg-white rounded-lg shadow-md py-14">
                    <Chart
                        options={options}
                        series={series}
                        type="line"
                        width="100%"
                    />
                </div>
                <div className="w-full flex-1 bg-white bg-gradient-to-r from-blue-500 to-violet-400 rounded-lg shadow-md py-14 relative">
                    <h1 className="text-center font-bold text-xl text-white -pt-6">Client Onboarding</h1>
                    <div className="text-5xl font-extrabold text-white absolute top-1/2 left-1/2 z-10 -translate-x-1/2 -translate-y-1/2">{series_donut[0]}%</div>
                    <Chart
                        options={options_donut}
                        series={series_donut}
                        type="donut"
                        width="100%"
                        height="100%"
                    />
                </div>
            </div>

            <div className="mb-4 w-full bg-white border border-gray-200 rounded-lg shadow-md relative overflow-hidden">
                <div className="absolute top-0 left-0 bottom-0 bg-primary bg-opacity-50 w-1/5 text-sm text-white flex flex-col items-center justify-center">
                    <h1 className="font-bold mb-2">Informaton d'accès</h1>
                    <ul>
                        <li>8 Rue rossigni, 37200</li>
                        <li>Étage 1: </li>
                        <li>Digicode</li>
                        <li>abro.landry@gmail.com</li>
                        <li>Propriétaire ou locataire</li>
                    </ul>
                </div>
                <iframe title="test" width="100%" height="250" id="gmap_canvas" src="https://maps.google.com/maps?q=2880%20Broadway,%20New%20York&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"/>
            </div>
            <div className="grid gap-3 grid-cols-1 md:grid-cols-2 lg:grid-cols-4 mb-4">

                <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-14">
                    <div className="flex flex-col items-center">
                        <img className="w-24 h-24 mb-10 rounded-lg" src="https://files.sbcdnsb.com/images/ipS43Y1gP2fOMAjGbfDI0Q/content/1573138650/957193/100/consultant-se_curite_.png" alt="al"/>
                        <span className="text-md text-gray-600 uppercase">Visual Designer</span>
                    </div>
                </button>

                <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-14">
                    <div className="flex flex-col items-center">
                        <img className="w-24 h-24 mb-10 rounded-lg" src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQq0-16LnfItCFB54C7W1BvcQ6wYtiBFGywP8Q78sV4SigvzSpQ" alt="al"/>
                        <span className="text-md text-gray-600 uppercase">formation label et certifications</span>
                    </div>
                </button>

                <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-14">
                    <div className="flex flex-col items-center">
                        <img className="w-24 h-24 mb-10 rounded-lg" src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRESoIIWhU0wIirrjgbTYPuUH2kqhJzrDsSBkl9n57T4V-RLW2S" alt="al"/>
                        <span className="text-md text-gray-600 uppercase">facture / devis</span>
                    </div>
                </button>

                <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-14">
                    <div className="flex flex-col items-center">
                        <img className="w-24 h-24 mb-10 rounded-lg" src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcS3zN4xiaeuIM2lGN3aPkGWsbHc3YdgsF_uYNO2iKRFk7gHfZmD" alt="al"/>
                        <span className="text-md text-gray-600 uppercase">mes messages</span>
                    </div>
                </button>

                <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-14">
                    <div className="flex flex-col items-center">
                        <img className="w-24 h-24 mb-10 rounded-lg" src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQDkPLPDYW6f76ZilyeEMlCemcwKQC-7QQc_YV9lo4JyOvOqb2f" alt="al"/>
                        <span className="text-md text-gray-600 uppercase">mes avis</span>
                    </div>
                </button>

                <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-14">
                    <div className="flex flex-col items-center">
                        <img className="w-24 h-24 mb-10 rounded-lg" src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSKzGp5EZNp_MaosnL5shAPj14jIKZL7iOphXxkEp_JCNhvLZyn" alt="al"/>
                        <span className="text-md text-gray-600 uppercase">ma comptabilité</span>
                    </div>
                </button>

                <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-14">
                    <div className="flex flex-col items-center">
                        <img className="w-24 h-24 mb-10 rounded-lg" src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQkvOCzlnLjhnc5nK7NaWDtlDnxbwXoeX9-yX043L79tFxws7Jg" alt="al"/>
                        <span className="text-md text-gray-600 uppercase">Mes offres</span>
                    </div>
                </button>

                <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-14">
                    <div className="flex flex-col items-center">
                        <img className="w-24 h-24 mb-10 rounded-lg" src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQ6fSV6EpBIWsMt75qC9pm64ROuvQi3XTzzjamp5egK04_lFMNq" alt="al"/>
                        <span className="text-md text-gray-600 uppercase">chiffres d'affaires</span>
                    </div>
                </button>

            </div>

            <div className="grid gap-3 grid-cols-1 lg:grid-cols-2 mb-10">
                <div className="block p-6 bg-white border border-gray-200 rounded-lg shadow-md">
                    <h5 className="mb-2 text-2xl font-bold tracking-tight text-primary">Mes stocks</h5>
                    <div className="font-normal text-gray-400">
                        <ul>
                            <li>Robinet</li>
                            <li>Serrure</li>
                            <li>Tuyau 30mm</li>
                        </ul>
                    </div>
                </div>
                <div className="block p-6 bg-white border border-gray-200 rounded-lg shadow-md">
                    <h5 className="mb-4 text-2xl font-bold tracking-tight text-primary">Commander vos fourniturres</h5>
                    <div>
                        <ul className="">
                            <li className="py-1 sm:py-2">
                                <div className="flex items-center space-x-4">
                                    <div className="flex-shrink-0">
                                        <img className="w-10 h-8 rounded" src="https://logodownload.org/wp-content/uploads/2017/05/leroy-merlin-logo-1.png" alt="merlin" />
                                    </div>
                                    <div className="flex-1 min-w-0">
                                        <p className="text-sm font-medium text-gray-400 truncate dark:text-white">
                                            Leroy Merlin
                                        </p>
                                    </div>
                                    <div className="inline-flex items-center text-base font-semibold">
                                        <button type="button" className="bg-blue-100 text-primary font-light mr-2 px-2.5 py-0.5 rounded-full">
                                            Commander
                                        </button>
                                    </div>
                                </div>
                            </li>
                            <li className="py-1 sm:py-2">
                                <div className="flex items-center space-x-4">
                                    <div className="flex-shrink-0">
                                        <img className="w-10 h-8 rounded" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Bricodepot.svg/1200px-Bricodepot.svg.png" alt="brocit depot" />
                                    </div>
                                    <div className="flex-1 min-w-0">
                                        <p className="text-sm font-medium text-gray-400 truncate dark:text-white">
                                            Bricot depot
                                        </p>
                                    </div>
                                    <div className="inline-flex items-center text-base font-semibold">
                                        <button type="button" className="bg-blue-100 text-primary font-light mr-2 px-2.5 py-0.5 rounded-full">
                                            Commander
                                        </button>
                                    </div>
                                </div>
                            </li>
                            <li className="py-1 sm:py-2">
                                <div className="flex items-center space-x-4">
                                    <div className="flex-shrink-0">
                                        <img className="w-10 h-8 rounded" src="https://www.ordissinaute.fr/sites/default/files/styles/full_new_main_no_crop/public/field/image/castorama-logo-1.png.jpg?itok=e_BaMOgt" alt="castorama" />
                                    </div>
                                    <div className="flex-1 min-w-0">
                                        <p className="text-sm font-medium text-gray-400 truncate dark:text-white">
                                            Castorama
                                        </p>
                                    </div>
                                    <div className="inline-flex items-center text-base font-semibold">
                                        <button type="button" className="bg-blue-100 text-primary font-light mr-2 px-2.5 py-0.5 rounded-full">
                                            Commander
                                        </button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <a href="#0" className="bg-blue-100 text-primary font-light mr-2 px-2.5 py-0.5 rounded-full">
                Une Question ? Besoin d'aide ? Consulter nous en lin
            </a>
        </div>
    );
}