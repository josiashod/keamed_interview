import React from "react";
import Header from "../components/header";
import LeftSideBar from "../components/left_sidebar";
import Sidebar from "../components/sidebar";
import Index from "./home";



function App() {
  return (
    <div className="w-full h-screen flex overflow-x-hidden lg:overflow-x-none bg-gray-100">
     <Sidebar />
     <div className="flex-1">
        <Header />
        <main className="px-8 lg:px-3 py-4 flex flex-col lg:flex-row">
          <div className="order-2 lg:order-1 lg:flex-1 pb-8">
            <Index />
          </div>
          <LeftSideBar />
        </main>
     </div>
     <button title="Contact"
        className="fixed z-90 bottom-10 right-8 bg-primary w-20 h-20 rounded-full drop-shadow-lg flex justify-center items-center text-white text-4xl hover:bg-blue-700 hover:drop-shadow-2xl hover:animate-bounce duration-300">&#9993;</button>
    </div>
  );
}

export default App;
