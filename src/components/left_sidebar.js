import React from "react";
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';

export default function LeftSideBar()
{
    return (
        <div className="w-full order-1 px-3 lg:w-2/6 text-justify">
            <div className="w-full mb-4 p-4 bg-white border rounded-lg shadow-md sm:p-8">
                <div class="flex flex-col items-center pb-10">
                    <h5 className="text-md mb-2 font-light leading-none text-gray-500">Profil completé: 65%</h5>
                    <img class="w-40 h-40 mb-3 ring-2 p-1 ring-gray-300  rounded-full shadow-lg" src="https://josiashod.github.io/my-team-page/images/photo5.png" alt="Bonnie"/>
                    <h5 class="mb-1 bg-blue-100 text-primary font-light px-2.5 py-0.5 rounded-full">Plombier Russo</h5>
                </div>
                <div className="flow-root border-t-2 border-dashed border-gray-300">
                    <ul className="divide-y-2 divide-dashed divide-gray-300 text-gray-400">
                        <li className="py-3 sm:py-4 border-t-1 border-dashed border-gray-300">
                            <span className="font-bold">Coordonées</span>
                            <ul className="ml-16 flex flex-col space-y-2">
                                <li>Telephone 1: <span className="text-primary font-light px-2.5 py-0.5 rounded-full ml-8">(+33)7 35 55 45 43</span></li>
                                <li>Telephone 2: <span className=" text-primary font-light px-2.5 py-0.5 rounded-full ml-8">(+33)7 35 55 45 43</span></li>
                                <li>Dévis: <span className="bg-blue-100 text-primary font-light px-2.5 py-0.5 rounded-full ml-8">Dépannage d'urgence</span></li>
                            </ul>
                        </li>
                        <li className="py-3 sm:py-4">
                            <span className="font-bold">Coordonées</span>
                            <ul className="ml-16 flex flex-col space-y-2">
                                <li>Telephone 1: <span className="text-primary font-light px-2.5 py-0.5 rounded-full ml-8">(+33)7 35 55 45 43</span></li>
                                <li>Telephone 2: <span className=" text-primary font-light px-2.5 py-0.5 rounded-full ml-8">(+33)7 35 55 45 43</span></li>
                                <li>Dévis: <span className="bg-blue-100 text-primary font-light px-2.5 py-0.5 rounded-full ml-8">Dépannage d'urgence</span></li>
                            </ul>
                        </li>
                        <li className="py-3 sm:py-4">
                            <span className="font-bold">Coordonées</span>
                            <ul className="ml-16 flex flex-col space-y-2">
                                <li>Telephone 1: <span className="text-primary font-light px-2.5 py-0.5 rounded-full ml-8">(+33)7 35 55 45 43</span></li>
                                <li>Telephone 2: <span className=" text-primary font-light px-2.5 py-0.5 rounded-full ml-8">(+33)7 35 55 45 43</span></li>
                                <li>Dévis: <span className="bg-blue-100 text-primary font-light px-2.5 py-0.5 rounded-full ml-8">Dépannage d'urgence</span></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

            <div className="bg-white shadow-lg rounded-lg mb-4 p-6">
                <div className="font-normal text-gray-400 mb-10">
                    <ul className="flex flex-col space-y-2">
                        <li>Corps de metier: <span className="text-black ml-5">Plombier</span></li>
                        <li>Équipement: <span className="text-black ml-5">Robineterie</span></li>
                        <li>Dévis: <span className="text-black ml-5">Dépannage d'urgence</span></li>
                    </ul>
                </div>
                <h3 className="text-primary mb-1">J'inscris mes disponibilité</h3>
                <span className="text-gray-400">Cochez les cases du jour où vous êtes disponible</span>
                <Calendar className="mt-4 rounded-md border-0 border-white min-w-full" defaultValue={new Date('02/02/21')} />
            </div>
            <div className="h-auto block p-6 bg-white border border-gray-200 rounded-lg shadow-md">
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-primary">Dernière interventions</h5>
                <p className="font-normal text-gray-400">Fuites d'eau: 12/10/21</p>
            </div>
        </div>
    );
}