import { Dropdown, Modal } from "flowbite-react";
import React, { useState } from "react";

export default function Header()
{
    const [show, setShow] = useState(false)
    const onClose = () => {
        setShow(false)
    }

    return (
        <header className="bg-white flex px-10 py-3 text-gray-800 border border-gray-200 w-full sticky top-0 z-50">
            <div>
                <h1 className="text-2xl font-semibold">Tableau de bord</h1>
                <span className="font-light text-gray-400">Lorem, ipsum dolor sit amet consectetur adipisicing elit.</span>
            </div>
            <nav className="flex-1 self-center flex justify-end space-x-6">
                <button type="button" className="p-2 hover:bg-gray-200 rounded-full">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="h-5 w-5" viewBox="0 0 16 16">
                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                    </svg>
                </button>
                <button type="button" className="p-2 hover:bg-gray-200 rounded-full" onClick={()=> {setShow(true)}}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="h-5 w-5" viewBox="0 0 16 16">
                        <path d="M5.5 2A3.5 3.5 0 0 0 2 5.5v5A3.5 3.5 0 0 0 5.5 14h5a3.5 3.5 0 0 0 3.5-3.5V8a.5.5 0 0 1 1 0v2.5a4.5 4.5 0 0 1-4.5 4.5h-5A4.5 4.5 0 0 1 1 10.5v-5A4.5 4.5 0 0 1 5.5 1H8a.5.5 0 0 1 0 1H5.5z"/>
                        <path d="M16 3a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
                    </svg>
                </button>
                <button type="button"  class="text-white bg-gray-100 text-md font-medium inline-flex items-center px-2.5 py-0.5 bg-gradient-to-b from-cyan-500 to-blue-500 rounded-full">
                    <svg aria-hidden="true" class="w-5 h-5 mr-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zm.995-14.901a1 1 0 1 0-1.99 0A5.002 5.002 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901z"/>
                    </svg>
                    15
                </button>
                <div className="flex space-x-1">
                    <img alt="pp" className="w-10 h-10 rounded-md" src="https://josiashod.github.io/my-team-page/images/photo5.png"/>
                    <div className="self-center">
                        <Dropdown
                            label="Plombier Brisso"
                            inline={true}
                        >
                            <Dropdown.Item>
                                Dashboard
                            </Dropdown.Item>
                            <Dropdown.Item>
                                Settings
                            </Dropdown.Item>
                            <Dropdown.Item>
                                Earnings
                            </Dropdown.Item>
                            <Dropdown.Item>
                                Sign out
                            </Dropdown.Item>
                        </Dropdown>
                    </div>
                </div>
            </nav>
            <Modal
                show={show}
                position="top-right"
                size="xl"
                onClose={onClose}
            >
                <Modal.Header />
                <Modal.Body>
                    <div className="grid gap-3 grid-cols-1 md:grid-cols-2 lg:grid-cols-4 mb-4">

                        <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-4">
                            <div className="flex flex-col items-center">
                                <img className="w-12 h-12 mb-10 rounded-lg" src="https://files.sbcdnsb.com/images/ipS43Y1gP2fOMAjGbfDI0Q/content/1573138650/957193/100/consultant-se_curite_.png" alt="al"/>
                                <span className="text-sm text-gray-600 uppercase">Visual Designer</span>
                            </div>
                        </button>

                        <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-4">
                            <div className="flex flex-col items-center">
                                <img className="w-12 h-12 mb-10 rounded-lg" src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQq0-16LnfItCFB54C7W1BvcQ6wYtiBFGywP8Q78sV4SigvzSpQ" alt="al"/>
                                <span className="text-sm text-gray-600 uppercase">formation label et certifications</span>
                            </div>
                        </button>

                        <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-4">
                            <div className="flex flex-col items-center">
                                <img className="w-12 h-12 mb-10 rounded-lg" src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRESoIIWhU0wIirrjgbTYPuUH2kqhJzrDsSBkl9n57T4V-RLW2S" alt="al"/>
                                <span className="text-sm text-gray-600 uppercase">facture / devis</span>
                            </div>
                        </button>

                        <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-4">
                            <div className="flex flex-col items-center">
                                <img className="w-12 h-12 mb-10 rounded-lg" src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcS3zN4xiaeuIM2lGN3aPkGWsbHc3YdgsF_uYNO2iKRFk7gHfZmD" alt="al"/>
                                <span className="text-sm text-gray-600 uppercase">mes messages</span>
                            </div>
                        </button>

                        <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-4">
                            <div className="flex flex-col items-center">
                                <img className="w-12 h-12 mb-10 rounded-lg" src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQDkPLPDYW6f76ZilyeEMlCemcwKQC-7QQc_YV9lo4JyOvOqb2f" alt="al"/>
                                <span className="text-sm text-gray-600 uppercase">mes avis</span>
                            </div>
                        </button>

                        <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-4">
                            <div className="flex flex-col items-center">
                                <img className="w-12 h-12 mb-10 rounded-lg" src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSKzGp5EZNp_MaosnL5shAPj14jIKZL7iOphXxkEp_JCNhvLZyn" alt="al"/>
                                <span className="text-sm text-gray-600 uppercase">ma comptabilité</span>
                            </div>
                        </button>

                        <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-4">
                            <div className="flex flex-col items-center">
                                <img className="w-12 h-12 mb-10 rounded-lg" src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQkvOCzlnLjhnc5nK7NaWDtlDnxbwXoeX9-yX043L79tFxws7Jg" alt="al"/>
                                <span className="text-sm text-gray-600 uppercase">Mes offres</span>
                            </div>
                        </button>

                        <button type="button" className="w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow-md hover:shadow-xl py-4">
                            <div className="flex flex-col items-center">
                                <img className="w-12 h-12 mb-10 rounded-lg" src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQ6fSV6EpBIWsMt75qC9pm64ROuvQi3XTzzjamp5egK04_lFMNq" alt="al"/>
                                <span className="text-sm text-gray-600 uppercase">chiffres d'affaires</span>
                            </div>
                        </button>

                    </div>
                </Modal.Body>
            </Modal>
        </header>
    );
}