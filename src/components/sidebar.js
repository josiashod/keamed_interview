import React from "react";


export default function Sidebar() {
  return (
    
    <aside className="sticky top-0 w-72 h-screen bg-primary overflow-hidden" aria-label="Sidebar">
        <div className="px-5 py-4 overflow-y-auto">
            <ul className="space-y-8 pt-40">
                <li>
                    <a href="#0" className="flex items-center p-2 text-base font-normal text-white rounded-lg hover:bg-gray-100 hover:text-primary">
                        <svg aria-hidden="true" className="flex-shrink-0 w-6 h-6 white transition duration-75 group-hover:text-primary" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM11 13a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z"></path></svg>
                        <span className="flex-1 ml-8 whitespace-nowrap">Accueil</span>
                    </a>
                </li>
                <li>
                    <a href="#0" className="flex items-center p-2 text-base font-normal text-white rounded-lg hover:bg-gray-100 hover:text-primary">
                        <svg aria-hidden="true" className="flex-shrink-0 w-6 h-6 white transition duration-75 group-hover:text-primary" fill="currentColor" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                            <path d="M14 9.5a2 2 0 1 1-4 0 2 2 0 0 1 4 0Zm-6 5.7c0 .8.8.8.8.8h6.4s.8 0 .8-.8-.8-3.2-4-3.2-4 2.4-4 3.2Z"/>
                            <path d="M2 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h5.243c.122-.326.295-.668.526-1H2a1 1 0 0 1-1-1V4a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v7.81c.353.23.656.496.91.783.059-.187.09-.386.09-.593V4a2 2 0 0 0-2-2H2Z"/>
                        </svg>
                        <span className="flex-1 ml-8 whitespace-nowrap">Consultation vidéo</span>
                    </a>
                </li>
                <li>
                    <a href="#0" className="flex items-center p-2 text-base font-normal text-white rounded-lg hover:bg-gray-100 hover:text-primary">
                        <svg aria-hidden="true" className="flex-shrink-0 w-6 h-6 white transition duration-75 group-hover:text-primary" fill="currentColor" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022l-.074.997zm2.004.45a7.003 7.003 0 0 0-.985-.299l.219-.976c.383.086.76.2 1.126.342l-.36.933zm1.37.71a7.01 7.01 0 0 0-.439-.27l.493-.87a8.025 8.025 0 0 1 .979.654l-.615.789a6.996 6.996 0 0 0-.418-.302zm1.834 1.79a6.99 6.99 0 0 0-.653-.796l.724-.69c.27.285.52.59.747.91l-.818.576zm.744 1.352a7.08 7.08 0 0 0-.214-.468l.893-.45a7.976 7.976 0 0 1 .45 1.088l-.95.313a7.023 7.023 0 0 0-.179-.483zm.53 2.507a6.991 6.991 0 0 0-.1-1.025l.985-.17c.067.386.106.778.116 1.17l-1 .025zm-.131 1.538c.033-.17.06-.339.081-.51l.993.123a7.957 7.957 0 0 1-.23 1.155l-.964-.267c.046-.165.086-.332.12-.501zm-.952 2.379c.184-.29.346-.594.486-.908l.914.405c-.16.36-.345.706-.555 1.038l-.845-.535zm-.964 1.205c.122-.122.239-.248.35-.378l.758.653a8.073 8.073 0 0 1-.401.432l-.707-.707z"/>
                            <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0v1z"/>
                            <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/>
                        </svg>
                        <span className="flex-1 ml-8 whitespace-nowrap">Historique</span>
                    </a>
                </li>
                <li>
                    <a href="#0" className="flex items-center p-2 text-base font-normal text-white rounded-lg hover:bg-gray-100 hover:text-primary">
                        <svg aria-hidden="true" className="flex-shrink-0 w-6 h-6 white transition duration-75 group-hover:text-primary" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z"/>
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                        </svg>
                        <span className="flex-1 ml-8 whitespace-nowrap">Mes rendez-vous</span>
                    </a>
                </li>
                <li>
                    <a href="#0" className="flex items-center p-2 text-base font-normal text-white rounded-lg hover:bg-gray-100 hover:text-primary">
                        <svg aria-hidden="true" className="flex-shrink-0 w-6 h-6 white transition duration-75 group-hover:text-primary" fill="currentColor" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13 0H6a2 2 0 0 0-2 2 2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2 2 2 0 0 0 2-2V2a2 2 0 0 0-2-2zm0 13V4a2 2 0 0 0-2-2H5a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1zM3 4a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V4z"/>
                        </svg>
                        <span className="flex-1 ml-8 whitespace-nowrap">Mes documents</span>
                    </a>
                </li>
                <li>
                    <a href="#0" className="flex items-center p-2 text-base font-normal text-white rounded-lg hover:bg-gray-100 hover:text-primary">
                        <svg aria-hidden="true" className="flex-shrink-0 w-6 h-6 white transition duration-75 group-hover:text-primary" fill="currentColor" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M5 1.5A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5v1A1.5 1.5 0 0 1 9.5 4h-3A1.5 1.5 0 0 1 5 2.5v-1Zm5 0a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-1Z"/>
                            <path d="M3 1.5h1v1H3a1 1 0 0 0-1 1V14a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V3.5a1 1 0 0 0-1-1h-1v-1h1a2 2 0 0 1 2 2V14a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V3.5a2 2 0 0 1 2-2Z"/>
                            <path d="M8 6.982C9.664 5.309 13.825 8.236 8 12 2.175 8.236 6.336 5.31 8 6.982Z"/>
                        </svg>
                        <span className="flex-1 ml-8 whitespace-nowrap">États des lieux</span>
                    </a>
                </li>
            </ul>
        </div>
        <div className="bottom">
            <div className="rotate-45 skew-y-4 square"></div>
            <div className="rotate-45 skew-y-4 square second"></div>
            <div className="rotate-45 skew-y-4 square third"></div>
        </div>
    </aside>

  );
}
